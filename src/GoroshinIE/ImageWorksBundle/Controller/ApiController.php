<?php

namespace GoroshinIE\ImageWorksBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ApiController extends Controller
{

    public function indexAction()
    {
        return $this->render('GoroshinIEImageWorksBundle:Api:get_all_images.html.twig', array(
            // ...
        ));
    }
    /**
     * @Route("/getAllImages")
     */
    public function getAllImagesAction()
    {
        return $this->render('GoroshinIEImageWorksBundle:Api:get_all_images.html.twig', array(
            // ...
        ));
    }

}
