<?php

namespace GoroshinIE\ImageWorksBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use GoroshinIE\ImageWorksBundle\Service\ImageProcessor;
use GoroshinIE\ImageWorksBundle\Entity\Image;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $imgProcessor = $this->get('image.processor');
        $img = new Image();
        $imgs = $imgProcessor->getAllImagesFromDb();
        $form = $this
                ->createFormBuilder($img)
                ->setAction('backend/add-file')
                ->add('file',FileType::class)
                ->add('submit',SubmitType::class)
                ->getForm();
        
        
        
        
        return $this->render('GoroshinIEImageWorksBundle:Default:imageworks.html.twig',['data'=>$imgs,'form'=>$form->createView()]);
    }
}
