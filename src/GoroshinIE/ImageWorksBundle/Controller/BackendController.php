<?php

namespace GoroshinIE\ImageWorksBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use GoroshinIE\ImageWorksBundle\Entity\Image;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BackendController extends Controller
{
    public function indexAction()
    {
        return $this->render('GoroshinIEImageWorksBundle:Backend:index.html.twig', array(
            // ...
        ));
    }

    public function getAllAction()
    {
         $imgs = $this->getDoctrine()
            ->getRepository(Image::class)
            ->findAll() ;
         
        $response = new JsonResponse($ImgsArray);
        return $response;

    }
    public function addFileAction(Request $img){
        $everything = $img->files->all();
        if(isset($everything['form']['file'])){
            $em = $this->getDoctrine()->getManager();
            $uploadedFile = $everything['form']['file'];
            $img = new Image();
            $img->setName($uploadedFile->getClientOriginalName());
            $img->setLink('img/'.$uploadedFile->getClientOriginalName());
            $img->setSignature(md5($uploadedFile->getClientOriginalName()));
            if($uploadedFile->move('/var/www/html/ImageWorks/web/img/',$uploadedFile->getClientOriginalName()))
            {
                $em->persist($img);
                if($em->flush())
                {
                    return $this->redirect('/', 301);
                }
            }
        }
        return new Response('failed'); 
    }
}
