<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace GoroshinIE\ImageWorksBundle\Service;
/**
 * Description of ImageProcessor
 *
 * @author durdom
 */

use GoroshinIE\ImageWorksBundle\Entity\Image;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


class ImageProcessor {
    
    public $em;
    public $imgModel = 'GoroshinIE\ImageWorksBundle\Entity\Image';
    
    public function __construct(EntityManager $em){
        $this->em = $em;   
    }
    
    public function getAllImagesFromDb(){
        return $this->em->getRepository($this->imgModel)->findAll();
    }
    
}
