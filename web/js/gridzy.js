/**
 * Gridzy v1.3
 *
 * Copyright 2016, Helmut Wandl
 */

var Gridzy = (function() {
	'use strict';

	var IMG_FAILURE = 0,
		IMG_LOADING = 1,
		IMG_LOADING_READY = 2, // the image is loading but we have width and height attributes for calculation
		IMG_READY = 3, // the image is still loading but we have naturalWidth and naturalHeight for calculation
		IMG_FINISHED = 4;

	var ITEM_REGISTERED = 1,
		ITEM_INSERT_QUEUE = 2,
		ITEM_INSERTED = 3;

	var idAutoIncrement = 0;

	var lazyLoadImagesQueue = null;

	var helper = {
		expectArray: function(i) {
			return helper.isArraylike(i) ? i : [i];
		},
		isArraylike: function(i) {
			return i.length === +i.length && typeof i !== 'string';
		},
		forceArrayObject: function(i) {
			if (!i) return [];
			if ('toArray' in Object(i)) return i.toArray();
			var length = i.length || 0, results = new Array(length);
			while (length--) results[length] = i[length];
			return results;
		},
		eachOfArray: function(arr, func, bind) {
			var a;
			for (a = 0; a < arr.length; a++) {
				bind ? func.apply(bind, [arr[a], a]) : func(arr[a], a);
			}
		},
		domWalk: function(node, func) {
			func(node);
			node = node.firstChild;
			while (node) {
				helper.domWalk(node, func);
				node = node.nextSibling;
			}
		},
		prependChild: function(parent, child) {
			if (parent.firstChild) {
				parent.insertBefore(child, parent.firstChild);
			} else {
				parent.appendChild(child);
			}
		},
		indexOfArray: !Array.prototype.indexOf ? // IE8-support
			function(arr, elt) {
				var a = 0,
					len = arr.length;

				for (; a < len; a++) {
					if (a in arr && arr[a] === elt) {
						return a;
					}
				}

				return -1;
			} :
			function(arr, elt) {
				return arr.indexOf(elt);
			},
		inArray: function(arr, elt) {
			return helper.indexOfArray(arr, elt) !== -1;
		},
		getComputedStyle: !window.getComputedStyle ? // IE8-support
			function(element) {
				return element.currentStyle;
			} :
			function(element) {
				return getComputedStyle(element);
			},
		addEventListener: !document.addEventListener ? // IE8-support
			function(element, name, func) {
				element.attachEvent('on' + name, function(event) { func.apply(element, [event]); });
			} :
			function(element, name, func) {
				element.addEventListener(name, func, false);
			},
		addClass: !document.createElement('div').classList ? // IE8- & IE9-support
			function(element, className) {
				if (!(element.className && -1 < element.className.search(new RegExp('(^|\\s+)' + className + '($|\\s+)')))) {
					element.className = (element.className + ' ' + className).replace(/\s+/, ' ').replace(/(^\s+|\s+$)/, '');
				}
			} :
			function(element, className) {
				element.classList.add(className);
			},
		removeClass: !document.createElement('div').classList ? // IE8- & IE9-support
			function(element, className) {
				element.className = element.className.replace(new RegExp('(^|\\s+)' + className + '($|\\s+)'), ' ').replace(/\s+/, ' ').replace(/(^\s+|\s+$)/, '');
			} :
			function(element, className) {
				element.classList.remove(className);
			},
		expectNaturalImageSize: (function() {
			var interval, images = [], update;

			update = function() {
				var a;
				for (a = 0; a < images.length; a++) {
					if (images[a].imgB.width && images[a].imgB.height) {
						if (images[a].img.naturalWidth === undefined) {
							images[a].img.naturalWidth = images[a].imgB.width;
						}
						if (images[a].img.naturalHeight === undefined) {
							images[a].img.naturalHeight = images[a].imgB.height;
						}
					}

					if (images[a].img.naturalWidth && images[a].img.naturalHeight || images[a].ready) {
						images[a].callback(images[a].img, !images[a].error);
						images.splice(a, 1);
						a--;
					}
				}

				if (!images.length) {
					clearInterval(interval);
					interval = null;
				}
			};

			return function(img, onReadyCallback) {
				var obj = {
					img: img,
					callback: onReadyCallback || function() {},
					imgB: document.createElement('img'),
					ready: false,
					error: false
				};

				helper.addEventListener(obj.imgB, 'load', function() { obj.ready = true; });
				helper.addEventListener(obj.imgB, 'error', function() { obj.ready = true; obj.error = true; });
				obj.imgB.src = obj.img.src;

				images.push(obj);
				if (!interval) interval = setInterval(update, 0);
			};
		})(),
		processLazyLoading: function() {
			if (lazyLoadImagesQueue === null) {
				return;
			}

			var pos = lazyLoadImagesQueue.length,
				viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
				viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0),
				imagesToLoad = [],
				rect,
				xPos,
				yPos,
				edge = 1.5; // 1 is exactly the viewport size; 2 is the space of one viewport more in each direction and so on

			while (pos--) {
				rect = lazyLoadImagesQueue[pos].image.getBoundingClientRect();

				// calculate visibility of the image - the values will be between 0 and 1 if the image is visible
				xPos = rect.right / (viewportWidth + (rect.right - rect.left));
				yPos = rect.bottom / (viewportHeight + (rect.bottom - rect.top));

				if (xPos > 1 - edge && xPos < edge && yPos > 1 - edge && yPos < edge) {
					imagesToLoad.push(lazyLoadImagesQueue.splice(pos, 1)[0]);
				}
			}

			// start loading images
			// we use "for" instead of "while", because we need the correct order. (The loading order should be from top to bottom)
			for (pos = 0; pos < imagesToLoad.length; pos++) {
				helper.addClass(imagesToLoad[pos].item.element, 'gridzyItemLoading');
				imagesToLoad[pos].item.element.appendChild(imagesToLoad[pos].item.progressIndicator);

				try {
					imagesToLoad[pos].image.naturalWidth = undefined; // IE8-support
					imagesToLoad[pos].image.naturalHeight = undefined; // IE8-support
				} catch(e) {}
				imagesToLoad[pos].image.src = imagesToLoad[pos].src;
			}
		}
	};

	function gridzy(element, options) {
		var isNewInstance = typeof element.gridzy === 'undefined',
			self = isNewInstance ? this : element.gridzy,
			prevContainerWidth = 0;

		// bind instance to container element
		element.gridzy = self;

		// initialize default options
		self._resetOptionParameters();

		if (isNewInstance) {
			// set unique instance id
			self.id = ++idAutoIncrement;

			self.itemIdAutoIncrement = 0;

			// initialize insertQueue parameters
			self._insertQueue = {
				maxQueueTime: 1000,
				maxQuietTime: 400,
				items: [],
				lastProcess: null,
				timer: null
			};

			self._forceRecalculateValues = false;

			// initialize user specific options
			self._updateOptionParameters(options);

			// set default parameters
			self._allItems = [];
			self._validItems = [];
			self._validItemsOptWidthAll = 0;
			self._validItemsOptionDesiredElementHeight = null;
			self._validItemsOptionHideBoxOnMissingImage = null;

			// register main element
			self._element = element;
			helper.addClass(self._element, 'gridzyMain');

			// create and insert the container box where the items will be positioned in.
			self._container = document.createElement('div');
			self._container.className = 'gridzyContainer';
			self._container.style.position = 'relative';
			self._container.style.padding = '0';
			self._container.style.border = '0';
			self._container.style.margin = '0';
			self._container.style.display = 'block';
			helper.prependChild(element, self._container);

			// append child elements to the container box
			self.append(element.children);

			// force rendering on window resize
			prevContainerWidth = self._container.clientWidth;
			helper.addEventListener(window, 'resize', function() {
				if (self._container.clientWidth !== prevContainerWidth) {
					prevContainerWidth = self._container.clientWidth;
					self.render();
				}
			});
		} else {
			self.setOptions(options);
		}

		return self;
	}

	gridzy.prototype = {
		setOptions: function(options) {
			var self = this;

			self._updateOptionParameters(options);

			self.render();
			helper.processLazyLoading();
		},
		getOption: function(optionName) {
			var self = this,
				result = null;

			switch(typeof self._options[optionName]) {

				// if the option value is a function get the return value of the function
				case 'function':
					result = self._options[optionName](self);
					break;

				// otherwise get the option value itself
				default:
					result = self._options[optionName];
			}

			return result;
		},
		render: function() {
			var self = this,
				renderObject = self._calculateGrid(), // get the new calculated values
				row,
				col,
				rowPos = renderObject.rows.length,
				colPos,
				autoFontSize = self.getOption('autoFontSize');

			self._options.onBeforeRender(self);

			// set all new values to the style attribute of elements
			while(rowPos--) {
				row = renderObject.rows[rowPos];
				colPos = row.items.length;
				while(colPos--) {
					col = row.items[colPos];
					col.element.style.cssText = ''
						+ 'position: absolute; '
						+ 'width: ' + col.displaySizeX + 'px; '
						+ 'height: ' + row.displaySizeY + 'px; '
						+ 'left: ' + col.displayPosX + 'px; '
						+ 'top: ' + row.displayPosY + 'px; '
						+ 'font-size: ' + (autoFontSize ? (col.displaySizeX * 100 / col.size[0]) + '%' : '100%') + '; ';
				}
			}

			self._container.style.height = renderObject.size[1] + 'px';

			self._options.onRender(self);
		},
		append: function(elements) {
			var self = this,
				containerPos;

			elements = helper.forceArrayObject(helper.expectArray(elements));

			containerPos = helper.indexOfArray(elements, self._container);
			if (containerPos > -1) {
				elements.splice(containerPos, 1);
			}

			if (self._insertQueue.items.length === 0) {
				self._insertQueue.lastProcess = (new Date()).getTime();
			}

			helper.eachOfArray(elements, function(contentElement) {
				contentElement.style.display = 'block';
				contentElement.style.visibility = 'hidden';

				// generate and insert the item object
				var item = {
					id: ++self.itemIdAutoIncrement,
					progressIndicator: document.createElement('span'),
					element: document.createElement('span'),
					contentElement: contentElement,
					size: [0, 0],
					ratio: 0,
					failures: false,
					status: ITEM_REGISTERED,
					images: [],
					imagesStatus: [],
					imagesInfo: []
				};

				helper.addClass(item.progressIndicator, 'gridzyItemProgressIndicator');
				helper.addClass(item.element, 'gridzyItem');
				helper.addClass(item.element, 'gridzyItemLoading');
				item.element.appendChild(item.progressIndicator);
				item.element.style.position = 'absolute';
				item.element.style.visibility = 'hidden';

				// search for images to get natural image sizes (they are needed to get the element size)
				helper.domWalk(contentElement, function(e) {
					if (e.nodeName === 'IMG') {
						item.images.push(e);

						var attributeWidth = e.getAttribute('width');
						var attributeHeight = e.getAttribute('height');
						var lazySrc = e.getAttribute('data-gridzylazysrc');
						var src = e.getAttribute('src');

						if (!src && lazySrc) {
							// if there is a data-gridzylazysrc attribute but there is no width or height attribute, we deactivate lazy loading for this image
							if (!attributeWidth || !attributeHeight) {
								e.src = lazySrc;
								lazySrc = null;
							} else {
								// set a transparent pixel as an image if there is no src
								e.src = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEHAAEALAAAAAABAAEAAAICTAEAOw==';
							}
						}

						item.imagesInfo.push({
							styleWidth: e.style.width,
							styleHeight: e.style.height,
							attributeWidth: attributeWidth,
							attributeHeight: attributeHeight,
							lazySrc: lazySrc
						});

						if (attributeWidth !== null && attributeHeight !== null && parseInt(attributeWidth) > 0 && parseInt(attributeHeight) > 0) {
							item.imagesStatus.push(IMG_LOADING_READY);
						} else {
							item.imagesStatus.push(IMG_LOADING);
						}
					}
				});
				helper.eachOfArray(item.images, function(image, index) {
					var onLoad = function() {
						item.imagesStatus[index] = image.naturalWidth ? IMG_FINISHED : IMG_FAILURE;

						if (!image.naturalWidth) { // IE8-support
							helper.expectNaturalImageSize(image, function(img, success) {
								item.imagesStatus[index] = success && image.naturalWidth ? (image.complete ? IMG_FINISHED : IMG_READY) : IMG_FAILURE;
								self._onImageReady(item);
							});
						}

						self._onImageFinished(item);
					};

					var onError = function() {
						item.imagesStatus[index] = IMG_FAILURE;
						self._onImageFinished(item);
					};

					helper.addEventListener(image, 'load', onLoad);
					helper.addEventListener(image, 'error', onError);

					if (image.complete) {
						onLoad();
					}

					if (!item.imagesInfo[index].lazySrc) {
						helper.expectNaturalImageSize(image, function(img, success) {
							item.imagesStatus[index] = success && image.naturalWidth ? (image.complete ? IMG_FINISHED : IMG_READY) : IMG_FAILURE;
							self._onImageReady(item);
						});
					}
				});

				self._allItems.push(item);
				self._container.appendChild(item.element);

				self._onImageReady(item);
				self._onImageFinished(item);
			});
		},
		_processInsertQueue: function(maxQuietTimeReached) {
			var self = this;

			var queueItem;
			var maxQueueTimeReached = self._insertQueue.lastProcess < (new Date()).getTime() - self._insertQueue.maxQueueTime;
			var imagesStylesToReset = [];
			var imagesStylesToResetPos;
			var imagesInfo;
			var imagePos;
			var imageStyle;
			var queueItems = [];
			var queueItemsPos;
			var preventInitialAnimation = self.getOption('preventInitialAnimation');
			var lazyLoadImages = [];

			if (self._insertQueue.timer !== null) {
				clearTimeout(self._insertQueue.timer);
				self._insertQueue.timer = null;
			}

			if (maxQuietTimeReached || maxQueueTimeReached) {

				// insert item elements
				queueItem = self._insertQueue.items.shift();
				while (queueItem) {
					queueItems.push(queueItem);
					helper.prependChild(queueItem.item.element, queueItem.contentElement);
					queueItem = self._insertQueue.items.shift();
				}

				// get images where to set width and height style to force image sizes from attributes
				queueItemsPos = queueItems.length;
				while (queueItemsPos--) {
					queueItem = queueItems[queueItemsPos];

					imagePos = queueItem.item.images.length;
					while (imagePos--) {
						imagesInfo = queueItem.item.imagesInfo[imagePos];

						// in some cases google chrome don't have the correct offset size even though the natural size is known
						if (!imagesInfo.attributeWidth && !queueItem.item.images[imagePos].offsetWidth && queueItem.item.images[imagePos].naturalWidth) {
							imagesInfo.attributeWidth = queueItem.item.images[imagePos].naturalWidth;
						}
						if (!imagesInfo.attributeHeight && !queueItem.item.images[imagePos].offsetHeight && queueItem.item.images[imagePos].naturalHeight) {
							imagesInfo.attributeHeight = queueItem.item.images[imagePos].naturalHeight;
						}

						if (imagesInfo.attributeWidth || imagesInfo.attributeHeight) {
							imageStyle = helper.getComputedStyle(queueItem.item.images[imagePos]);

							if ((parseInt(imageStyle.width) < 40 || imageStyle.width === 'auto' || imagesInfo.lazySrc) && imagesInfo.attributeWidth && !imagesInfo.styleWidth) {
								imagesStylesToReset.push({image:queueItem.item.images[imagePos], definition:'width', value:imagesInfo.attributeWidth + 'px'});
							}
							if ((parseInt(imageStyle.height) < 40 || imageStyle.height === 'auto' || imagesInfo.lazySrc) && imagesInfo.attributeHeight && !imagesInfo.styleHeight) {
								imagesStylesToReset.push({image:queueItem.item.images[imagePos], definition:'height', value:imagesInfo.attributeHeight + 'px'});
							}
						}

						// get images to lazy load
						if (imagesInfo.lazySrc) {
							lazyLoadImages.push({item: queueItem.item, image:queueItem.item.images[imagePos], src: imagesInfo.lazySrc});
						}
					}
				}

				// set width and height style to force image sizes from attributes
				imagesStylesToResetPos = imagesStylesToReset.length;
				while (imagesStylesToResetPos--) {
					imagesStylesToReset[imagesStylesToResetPos].image.style[imagesStylesToReset[imagesStylesToResetPos].definition] = imagesStylesToReset[imagesStylesToResetPos].value;

					// set display to "none" because FF need more than only a browser rerendering to apply the sizes to elements
					imagesStylesToReset[imagesStylesToResetPos].image.style.display = 'none';
				}

				// force browser rendering and display images again after hiding it because of FF
				document.body.offsetWidth;
				imagesStylesToResetPos = imagesStylesToReset.length;
				while (imagesStylesToResetPos--) {
					imagesStylesToReset[imagesStylesToResetPos].image.style.display = '';
				}

				// get size of item element
				queueItemsPos = queueItems.length;
				while (queueItemsPos--) {
					queueItem = queueItems[queueItemsPos];
					queueItem.item.size = [queueItem.contentElement.offsetWidth, queueItem.contentElement.offsetHeight];
					queueItem.item.ratio = queueItem.item.size[0] / queueItem.item.size[1];
				}

				// remove width and height style where it was set before getting size of item element
				imagesStylesToResetPos = imagesStylesToReset.length;
				while (imagesStylesToResetPos--) {
					imagesStylesToReset[imagesStylesToResetPos].image.style[imagesStylesToReset[imagesStylesToResetPos].definition] = '';
				}

				// set styles for item element
				queueItemsPos = queueItems.length;
				while (queueItemsPos--) {
					queueItem = queueItems[queueItemsPos];

					helper.addClass(queueItem.contentElement, 'gridzyItemContent');
					queueItem.contentElement.style.width = '100%';
					queueItem.contentElement.style.height = '100%';
					queueItem.item.status = ITEM_INSERTED;
				}

				// deactivate animation during initial rendering by changing the class name - only if is set in options
				if (preventInitialAnimation) {
					self._element.className = self._element.className.replace(/(^|\s)gridzyAnimated(\s|$)/, '$1gridzyAnimatedOnLoad$2');
				}

				// calculate and render grid
				self._forceRecalculateValues = true;
				self.render();

				// reactivate animations after initial rendering by reset the class name
				if (preventInitialAnimation) {
					document.body.offsetWidth; // force rendering before we activate animation again
					self._element.className = self._element.className.replace(/(^|\s)gridzyAnimatedOnLoad(\s|$)/, '$1gridzyAnimated$2');
				}

				// make item elements visible
				queueItemsPos = queueItems.length;
				while (queueItemsPos--) {
					queueItem = queueItems[queueItemsPos];
					queueItem.item.element.style.visibility = '';
					queueItem.item.contentElement.style.visibility = '';
				}

				// register lazy loading images
				if (lazyLoadImages.length) {
					self._addImagesToLazyLoadQueue(lazyLoadImages);
				}

				self._insertQueue.lastProcess = (new Date()).getTime();
			} else {
				self._insertQueue.timer = setTimeout(function() {
					self._processInsertQueue(true);
				}, self._insertQueue.maxQuietTime);
			}
		},
		_addImagesToLazyLoadQueue: function(images) {
			// activate lazy loading if is not already activated
			if (lazyLoadImagesQueue === null && images.length) {
				lazyLoadImagesQueue = [];
				helper.addEventListener(window, 'resize', helper.processLazyLoading);
				helper.addEventListener(window, 'scroll', helper.processLazyLoading);
			}

			// add images to lazy loading queue
			lazyLoadImagesQueue = lazyLoadImagesQueue.concat(images);

			helper.processLazyLoading();
		},
		_addToInsertQueue: function(contentElement, item) {
			this._insertQueue.items.push({contentElement: contentElement, item: item});
			this._processInsertQueue(false);
		},
		_onImageFinished: function(item) {
			var self = this,
				failures = item.failures;

			if (!helper.inArray(item.imagesStatus, IMG_LOADING) && !helper.inArray(item.imagesStatus, IMG_LOADING_READY) && !helper.inArray(item.imagesStatus, IMG_READY)) {
				if (helper.inArray(item.imagesStatus, IMG_FAILURE)) {
					item.failures = true;
				}
				if (item.status === ITEM_INSERTED && failures !== item.failures) {
					self._forceRecalculateValues = true;
					self.render();
				}

				helper.removeClass(item.element, 'gridzyItemLoading');
				if (item.progressIndicator && item.progressIndicator.parentNode && item.progressIndicator.parentNode === item.element) {
					item.element.removeChild(item.progressIndicator);
				}
			}
		},
		_onImageReady: function(item) {
			var self = this,
				failures = item.failures;

			// register image loading failures
			if (helper.inArray(item.imagesStatus, IMG_FAILURE)) {
				item.failures = true;
			} else {
				item.failures = false;
			}
			if (item.status === ITEM_INSERTED && failures !== item.failures) {
				self._forceRecalculateValues = true;
				self.render();
			}

			// insert the element if the sizes of all contained images are known (because the size is important to calculate the grid)
			if (item.status === ITEM_REGISTERED && !helper.inArray(item.imagesStatus, IMG_LOADING)) {
				item.status = ITEM_INSERT_QUEUE;
				self._addToInsertQueue(item.contentElement, item);
			}
		},
		_resetOptionParameters: function() {
			this._options = {
				spaceBetweenElements: 1,
				desiredElementHeight: 200,
				autoFontSize: false,
				hideBoxOnMissingImage: true,
				preventInitialAnimation: false,
				onBeforeOptionsChanged: function() {},
				onOptionsChanged: function() {},
				onBeforeRender: function() {},
				onRender: function() {}
			};
		},
		_updateOptionParameters: function(options) {
			var self = this;

			options = options || {};

			self._options.onBeforeOptionsChanged(self);

			// set options
			if (typeof options.spaceBetweenElements !== 'undefined') self._options.spaceBetweenElements = typeof options.spaceBetweenElements === 'function' ? options.spaceBetweenElements : +options.spaceBetweenElements;
			if (typeof options.desiredElementHeight !== 'undefined') self._options.desiredElementHeight = typeof options.desiredElementHeight === 'function' ? options.desiredElementHeight : +options.desiredElementHeight;
			if (typeof options.autoFontSize !== 'undefined') self._options.autoFontSize = typeof options.autoFontSize === 'function' ? options.autoFontSize : !!options.autoFontSize;
			if (typeof options.hideBoxOnMissingImage !== 'undefined') self._options.hideBoxOnMissingImage = typeof options.hideBoxOnMissingImage === 'function' ? options.hideBoxOnMissingImage : !!options.hideBoxOnMissingImage;
			if (typeof options.preventInitialAnimation !== 'undefined') self._options.preventInitialAnimation = typeof options.preventInitialAnimation === 'function' ? options.preventInitialAnimation : !!options.preventInitialAnimation;
			if (typeof options.onBeforeOptionsChanged === 'function') self._options.onBeforeOptionsChanged = options.onBeforeOptionsChanged;
			if (typeof options.onOptionsChanged === 'function') self._options.onOptionsChanged = options.onOptionsChanged;
			if (typeof options.onBeforeRender === 'function') self._options.onBeforeRender = options.onBeforeRender;
			if (typeof options.onRender === 'function') self._options.onRender = options.onRender;

			self._options.onOptionsChanged(self);
		},
		_precalculateValues: function(onlyIfOptionsChanged) {
			var self = this,
				allItems = self._allItems,
				item,
				validItems = [],
				optWidthAll = 0,
				loopPos = allItems.length,
				desiredElementHeight = self.getOption('desiredElementHeight'),
				hideBoxOnMissingImage = self.getOption('hideBoxOnMissingImage');

			if (onlyIfOptionsChanged && self._validItemsOptionDesiredElementHeight === desiredElementHeight && self._validItemsOptionHideBoxOnMissingImage === hideBoxOnMissingImage) {
				return;
			}

			while(loopPos--) {
				item = allItems[loopPos];
				if (item.status !== ITEM_INSERTED) {
					continue;
				}
				if (hideBoxOnMissingImage && item.failures) {
					item.element.style.display = 'none';
				}
				else if (item.size[0] && item.size[1]) {
					item.element.style.display = '';
					validItems.unshift(item);
					item.optWidth = desiredElementHeight * item.size[0] / item.size[1];
					optWidthAll += item.optWidth;
				}
			}

			self._validItemsOptionDesiredElementHeight = desiredElementHeight;
			self._validItemsOptionHideBoxOnMissingImage = hideBoxOnMissingImage;
			self._validItems = validItems;
			self._validItemsOptWidthAll = optWidthAll;
			self._forceRecalculateValues = false;
		},
		_calculateGrid: function() {
			var self = this,
				loopPos,
				loopPos2,
				items,
				itemsLength,
				item,
				spaceBetweenElements = self.getOption('spaceBetweenElements'),
				containerWidth = self._container.clientWidth,
				optWidthAll,
				optWidthRow,
				curWidthRow = 0,
				renderObject = {
					rows: [],
					size: [containerWidth, 0]
				},
				rows = renderObject.rows,
				rowsLength,
				row,
				rowLength,
				prevRow,
				displaySizeAllX = 0,
				displaySizeAllY = 0,
				containerWidthWithoutSpaces;

			// get valid items and optimal width of all items
			this._precalculateValues(!this._forceRecalculateValues);
			items = self._validItems;
			itemsLength = items.length;
			optWidthAll = self._validItemsOptWidthAll;

			// find the optimal length (pixels) of a row
			optWidthRow = optWidthAll / (Math.round((optWidthAll + (spaceBetweenElements * itemsLength)) / (containerWidth + spaceBetweenElements)) || 1);

			// attach items to rows
			loopPos = itemsLength;
			while(loopPos--) {
				item = items[loopPos];
				if (curWidthRow + (item.optWidth / 2) > optWidthRow * rows.length) {
					rows.unshift({
						displaySizeY: 0,
						displayPosY: 0,
						ratioAll: 0,
						items: []
					});
				}
				curWidthRow += item.optWidth;
				rows[0].ratioAll += item.ratio;
				rows[0].items.unshift(item);
			}

			// calculate sizes and positions of elements
			rowsLength = rows.length;
			prevRow = null;
			for (loopPos = 0; loopPos < rowsLength; loopPos++) {
				row = rows[loopPos];
				displaySizeAllX = 0;
				displaySizeAllY = 0;
				rowLength = row.items.length;
				containerWidthWithoutSpaces = containerWidth - ((rowLength - 1) * spaceBetweenElements);

				for (loopPos2 = 0; loopPos2 < rowLength; loopPos2++) {
					item = row.items[loopPos2];
					item.displaySizeX = Math.round((item.ratio / row.ratioAll) * containerWidthWithoutSpaces);
					item.displayPosX = displaySizeAllX + (loopPos2 * spaceBetweenElements);
					displaySizeAllX += item.displaySizeX;

					if (loopPos2 === rowLength - 1 && displaySizeAllX !== containerWidthWithoutSpaces) {
						item.displaySizeX += containerWidthWithoutSpaces - displaySizeAllX;
					}
					displaySizeAllY += item.displaySizeX / item.ratio;
				}

				row.displaySizeY = Math.round(displaySizeAllY / rowLength);
				row.displayPosY = prevRow ? prevRow.displayPosY + prevRow.displaySizeY + spaceBetweenElements : 0;

				renderObject.size[1] += row.displaySizeY + (loopPos ? spaceBetweenElements : 0);

				prevRow = row;
			}

			return renderObject;
		}
	};

	// if jQuery is available register Gridzy as a plugin
	if (typeof jQuery !== 'undefined' && typeof jQuery.fn !== 'undefined') {
		jQuery.fn.gridzy = function(options) {
			return this.each(function() {
				jQuery(this).data('gridzy', new gridzy(this, options));
			});
		};
	}

	return gridzy;
})();
